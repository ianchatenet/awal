import{createTmpArr,addSeed} from './fonctions.js';
import{winningMove} from './verification_coup_gagnant.js';
import{verif,createClickElms,removeClick,removeClickElems} from './turnperturn.js';
import{resetGame} from './reset_game.js';

export function init(){
    
    let box = document.getElementsByClassName("box");
    let boxId =[];   
    let player = true;
   
    
    for(let i=0;i<box.length;i++){
        boxId.push(box[i].id);             //create an array with elements's id      
    }
    click();
    function click(){
        let scoreR=document.getElementById("scoreR").innerHTML;
        let scoreB=document.getElementById("scoreB").innerHTML;
        if(scoreR>24){
            alert(`le joueur rouge gagne avec ${scoreR} graines`)
            return;
        }
        if(scoreB>24){
            alert(`le joueur bleu gagne avec ${scoreB} graines`)
            return;
        }
        let arrClick = verif(player);
        createClickElms(arrClick,system); // create click event on elements
        
        function system(event){                      //get content of the clicked element and his id
            let id = event.target.id;
            let content = parseInt(event.target.innerHTML);
            event.target.innerHTML = 0;                            //clicked element become empty   
            
            function checkId(element){        //filter with the clicked id
                return element == id;
            }
            let indexTmp = boxId.findIndex(checkId);           //get index of clicked element
            let arrTmp = createTmpArr(indexTmp,box);                //create temporary array containing all element starting clicked element
            let lastElem=addSeed(arrTmp,content);       // adding seed on next elements   
           
            winningMove(lastElem,player);
            player = !player;
            let arrRemove = removeClickElems(player);
            removeClick(arrRemove,system);              //remove click event on elements
            click();
        }  
        
    }
    resetGame();

}