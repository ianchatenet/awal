

export function verif(player){
    let arrClick;
    if(player==true){
        arrClick = document.getElementsByClassName("bot");
        document.getElementById('player').classList.remove('red');
        document.getElementById("player").classList.add('blue');
        document.getElementById("player").innerHTML="joueur Bleu";
        document.getElementById("blueP").classList.remove("opac");  
        document.getElementById("redP").classList.add("opac");
    }
    if(player==false){  
        arrClick = document.getElementsByClassName("top");
        document.getElementById("player").innerHTML="joueur Rouge"; 
        document.getElementById('player').classList.remove('blue');
        document.getElementById("player").classList.add('red');
        document.getElementById("redP").classList.remove("opac");
        document.getElementById("blueP").classList.add("opac");
    }
    return arrClick;
}

export function createClickElms(arrClick,system){
    for(let i=0;i<arrClick.length;i++){                     
        if(arrClick[i].innerHTML>0)  
    arrClick[i].addEventListener('click', system);
    }
}

export function removeClickElems(player){
    let arrRemove;
    if(player==true){
        arrRemove = document.getElementsByClassName("top");
    }
    if(player==false){  
        arrRemove = document.getElementsByClassName("bot");
    }
    return arrRemove;
}
export function removeClick(arrRemove,system){
    for(let i=0;i<arrRemove.length;i++){
        arrRemove[i].removeEventListener('click',system);
    }
}